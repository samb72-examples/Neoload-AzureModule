﻿<#
.Synopsis
   Connects to Azure with Neoload application user and loads all required config variables
.DESCRIPTION
   Connects to Azure with Neoload application user and loads all required config variables
   Variables are stored in NeoloadConfiguration.xml which needs to be in the same folder as the Cmdlet.
.EXAMPLE
   Connect-NeoloadFarm

#>
function Connect-NeoloadFarm
{

    Begin
    {
    }
    Process
    {
      Import-Module -Name AzureRM

      $modulePath = $PSScriptRoot
      $configFile = 'NeoloadConfiguration.xml'
      $configFilePath = Join-Path -Path $modulePath -ChildPath $configFile

      # Try and load the configuration data

      [xml]$configData = Get-Content -Path $configFilePath
      $configDataNodes = $configData.ChildNodes

      $appID = $configDataNodes.AppID
      $appPW =  ConvertTo-SecureString $configDataNodes.AppPW -AsPlainText -Force
      $tenantID = $configDataNodes.TenantID

      # Try and connect to Azure. If successful, return the config data as a result.
      # If not, return null.

      $Error.Clear()

      $appCred = New-Object System.Management.Automation.PSCredential($appID, $appPW) 
      $azureAccount = Add-AzureRmAccount -TenantId $tenantID -Credential $appCred -ServicePrincipal

      if($Error.Count -gt 0) { return $null }
      else { return $configDataNodes }
    }
    End
    {
    }
}



<#
.Synopsis
   Return an array of all Public IP addresses that can be or are already in use by the NeoloadFarm
.DESCRIPTION
   Return an array of all Public IP addresses that can be or are already in use by the NeoloadFarm
.EXAMPLE
   Get-NeoloadFarmIPAddress
#>
function Get-NeoloadFarmIPAddress
{
    Begin
    {
    }
    Process
    {
      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      }

      $resGroup = $neoloadData.ResourceGroupName
      $resTag = $neoloadData.ResourceTag

      $neoloadIPs = Find-AzureRmResource -TagName 'Owner' -TagValue $resTag | Where-Object { $_.ResourceType -eq 'Microsoft.Network/publicIPAddresses' } | Get-AzureRmPublicIpAddress
      $neoloadIPs = $neoloadIPs | Select-Object Name, Location, ResourceGuid, PublicIpAllocationMethod, IPAddress, IPConfiguration, @{Name='Associated';Expression={$_.IPConfiguration -ne $null}}, Id
      return $neoloadIPs


    }
    End
    {
    }
}


<#
.Synopsis
   Adds Public IP addresses for the Neoload Azure Farm
.DESCRIPTION
   Adds Public IP addresses for the Neoload Azure Farm.
   The number specified in the parameter is the total required. So if 15 is specified and 10 are already allocated, then only 5 more will be created.
   If the number specified is greater than the number already allocated then a message will be output saying no more need to be created and nothing
   will be done.
.EXAMPLE
   Set-NeoloadFarmIPAddress -RequiredIPAddresses 20
#>
function Set-NeoloadFarmIPAddress
{
    Param
    (
        # The total number of Public IP addresses required in the Neoload Farm
        [Parameter(Mandatory,
                   Position=0)]
        [int]$RequiredIPAddresses
    )

    Begin
    {
    }
    Process
    {
      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm

      $resGroup = $neoloadData.ResourceGroupName
      $location = 'CanadaCentral'
      $ownerTag = $neoloadData.ResourceTag

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      }

      # Display warning about firewall requests
      Write-Warning "Please be aware that any new IP addresses will require firewall rules to be requested."

      $maxIPs = $neoloadData.MaxPublicIPs
      $currentIPs = @(Get-NeoloadFarmIPAddress).count
      
      Write-Verbose "Currently $currentIPs public IP addresses allocated."

      if($RequiredIPAddresses -ge $maxIPs)
      {
        Write-Warning "The number specified is above the maximum number of IP Addresses allowed. Maximum allowed is $maxIPs. Only $maxIPs will be allocated."
        $RequiredIPAddresses = $maxIPs
      }

      if($RequiredIPAddresses -le $currentIPs)
      {
        Write-Output 'The farm already has the required number of IP Addresses'
        Break
      }

      $ipDiff = $RequiredIPAddresses - $currentIPs

      Write-Verbose "$ipDiff more public ip addresses will be allocated."

      $newIPAddresses = @()

      for($i = 1; $i -le $ipDiff; $i++)
      {
        Write-Verbose 'Creating IP Address...'
        
        
        # Generate a name for the IP address as this is required. Loop five times trying to generate a unique name. After that throw an error.
        $randomID = Get-Random -Minimum 10000 -Maximum 100000
        $ipName = 'NeoloadPublicIP' + $randomID
        $loopCount = 1
        
        while( (Find-AzureRmResource -ResourceNameContains $ipName -ResourceGroupNameContains $resGroup ) -and $loopCount -lt 5)
        {
          $randomID = Get-Random -Minimum 10000 -Maximum 100000
          $ipName = 'NeoloadPublicIP' + $randomID
          $loopCount++
        }

        if($loopCount -eq 5)
        {
          Throw 'Unable to generate unique name for IP Address. Will not create.'
          Break
        }

        Write-Verbose "Running Command New-AzureRmPublicIpAddress -Name $ipName -AllocationMethod Static -ResourceGroupName $resGroup -Location $location -Tag @{'Owner' = $ownerTag} -IpAddressVersion IPv4"
        $newIPAddresses += New-AzureRmPublicIpAddress -Name $ipName -AllocationMethod Static -ResourceGroupName $resGroup -Location $location -Tag @{'Owner' = $ownerTag} -IpAddressVersion IPv4
      
      }


    Return $newIPAddresses



    }
    End
    {
    }
}




<#
.Synopsis
   Returns an array of all servers in the farm, their size, IP address and status 
.DESCRIPTION
   Returns an array of all servers in the farm, their size, IP address and status
.EXAMPLE
   Get-NeoloadFarm
#>
function Get-NeoloadFarm
{

    Begin
    {
    }
    Process
    {

      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      }

      $resGroup = $neoloadData.ResourceGroupName
      $resTag = $neoloadData.ResourceTag

      # Find all VMs that are a member of the Neoload Farm
      $neoloadVMs = Find-AzureRmResource -TagName 'Owner' -TagValue $resTag | Where-Object { $_.ResourceType -eq 'Microsoft.Compute/virtualMachines' } | Get-AzureRmVM
      $neoloadVMs = $neoloadVMs | Select-Object Name, Location, ResourceGroupName, @{Name='Size' ; Expression={$_.HardwareProfile.VmSize }}

      # Get Public IP Address and VM Status for each VM

      foreach($vm in $neoloadVMs)
      {
        $vmNetInterface = Get-AzureRmNetworkInterface | Where-Object { $_.VirtualMachine.Id -like "*$($vm.Name)*" }
        $vmPublicIP = (Get-AzureRmPublicIpAddress | Where-Object { $_.IpConfiguration.Id -like "*$($vmNetInterface.Name)*" }).IpAddress

        $vm | Add-Member -MemberType NoteProperty -Name PublicIP -Value $vmPublicIP

        $vmStatus = ($vm | Get-AzureRmVM -Status).Statuses[1].DisplayStatus

        $vm | Add-Member -MemberType NoteProperty -Name Status -Value $vmStatus
      }

      return $neoloadVMs

    }
    End
    {
    }
}



<#
.Synopsis
   Provisions a new Load Generator from the Load Generator template.
.DESCRIPTION
   Provisions a new Load Generator from the Load Generator template.
   The number to generate can be passed as a parameter. If the number is greater than the 
   current number of Public IP addresses allocated then generators will only be
   created for the number of free IP addresses.
.EXAMPLE
   New-NeoloadLoadGenerator -GeneratorsToProvision 3
#>
function New-NeoloadLoadGenerator
{
    Param
    (
        # Number of generators to create
        [Parameter(Mandatory,
                   Position=0)]
        [int]$GeneratorsToProvision
    )

    Begin
    {
    }
    Process
    {

      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      }

      # Data needed for creation of new VM
      $resGroup = $neoloadData.ResourceGroupName
      $resTag = $neoloadData.ResourceTag
      $imagePrefix = $neoloadData.ImageNamePrefix
      $generatorPrefix = $neoloadData.GeneratorNamePrefix
      $location = $neoloadData.Location
      $vNetwork = $neoloadData.VirtualNetwork
      $nsgName = $neoloadData.NetworkSecurityGroup
      $vmSize = $neoloadData.VMSize
      $vmPassword = ConvertTo-SecureString -String $neoloadData.GeneratorPassword -AsPlainText -Force
      $vmUserName = $neoloadData.GeneratorUserName
      $vmCred = New-Object System.Management.Automation.PSCredential ($vmUserName, $vmPassword); 

      # Determine number of free IP addresses and check that we can provision enough load generators. If not, then set the number to provision to be the number of free IP addresses.

      $freeIPAddresses = @(Get-NeoloadFarmIPAddress | Where-Object { $_.Associated -eq $false })
      $freeIPAddressCount = $freeIPAddresses.count

      Write-Verbose "$freeIPAddressCount un-associated IP addresses available for use."

      if($freeIPAddressCount -lt $GeneratorsToProvision)
      {
        Write-Warning "Generators to provision ($GeneratorsToProvision) is greater than the number of free IP addresses ($freeIPAddressCount). Only $freeIPAddressCount generators will be provisioned. If more are required, please allocate more IP addresses using Set-NeoloadFarmIPAddress."
        $GeneratorsToProvision = $freeIPAddressCount
      }

      # Provision generators from the latest template
      
      $vmImage = Find-AzureRmResource -TagName 'Owner' -TagValue $resTag -ExpandProperties | Where-Object { $_.ResourceType -eq 'Microsoft.Compute/images' -and $_.Name -like "*$imagePrefix*" -and $_.ResourceGroupName -eq $resGroup } | Get-AzureRMImage
            
      $newVMs = @()

      for($i = 0; $i -lt $GeneratorsToProvision; $i++)
      {
        # Generate VM name. Try five times and if a unique name cannot be created then skip.

        $randomID = Get-Random -Minimum 10000 -Maximum 100000
        $vmName = $generatorPrefix + $randomID
        $loopCount = 1
        
        while( (Find-AzureRmResource -ResourceNameContains $vmName -ResourceGroupNameContains $resGroup ) -and $loopCount -lt 5)
        {
          $randomID = Get-Random -Minimum 10000 -Maximum 100000
          $vmName = $generatorPrefix + $randomID
          $loopCount++
        }

        # Set private network configuration
        #$vNetName = "$vmName-Internal"
        $subnetIndex = 0
        $vNet = Get-AzureRmVirtualNetwork -Name $vNetwork -ResourceGroupName $resGroup

        # Set NIC Config
        $nicName = "$vmName-NIC"
        $nsg = Get-AzureRmNetworkSecurityGroup -Name $nsgName -ResourceGroupName $resGroup
        $nic = New-AzureRmNetworkInterface -Name $nicName -ResourceGroupName $resGroup -Location $location -SubnetId $vNet.Subnets[$subnetIndex].Id -PublicIpAddressId $freeIPAddresses[$i].Id -Tag @{'Owner' = $resTag} -NetworkSecurityGroupId $nsg.id

        # Set VM Config
        $vm = New-AzureRmVMConfig -VMName $vmName -VMSize $vmSize
        $vm = Set-AzureRMVMSourceImage -VM $vm -Id $vmImage.Id
        $vm = Set-AzureRmVMOSDisk -VM $vm -CreateOption FromImage
        $vm = Set-AzureRmVMOperatingSystem -VM $vm -Linux -ComputerName $vmName -Credential $vmCred
        $vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id
        
        $null = Set-AzureRmVMBootDiagnostics -VM $vm -Disable
            

        # Create the VM

        $vmCreated = New-AzureRmVM -ResourceGroupName $resGroup -Location $location -VM $vm -Tags @{'Owner' = $resTag }
        
        if($vmCreated.StatusCode -eq 'OK')
        {
          $newVMs += Get-AzureRmVM -Name $vmName -ResourceGroupName $resGroup
        }
      
      }



      return $newVMs



    }
    End
    {
    }
}


<#
.Synopsis
   This cmdlet shuts down a single or multiple Neoload Generators
.DESCRIPTION
   This cmdlet shuts down a single or multiple Neoload Generators. This has been created as a mechanism
   to enable to easily process these commands as jobs. It also enables shutting down indivudal
   servers if required.
.EXAMPLE
   Stop-NeoloadGenerator -Name 'NeoloadGenerator13440'
.EXAMPLE
  @('Server1', 'Server2') | Stop-NeoloadGenerator
#>
function Stop-NeoloadGenerator
{
    Param
    (
        # Neoload Generator Name
        [Parameter(Mandatory=$true,
                   ValueFromPipeline,
                   Position=0)]
        $Name
    )

    Begin
    {
      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm
      
      $resTag = $neoloadData.ResourceTag
      $resGroup = $neoloadData.ResourceGroupName

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      }
    }
    Process
    {
      # Get VM Object

      Write-Verbose "Retrieving VM Object for $Name"
      $neoloadVM = Get-AzureRMVM -ResourceGroupName $resGroup -Name $Name

      # Shutdown VM

      Write-Verbose "Shutting down $Name"
      return $neoloadVM | Stop-AzureRmVM -Force

    }
    End
    {
    }
}

<#
.Synopsis
   This cmdlet starts up a single or multiple Neoload Generators
.DESCRIPTION
   This cmdlet starts up a single or multiple Neoload Generators. This has been created as a mechanism
   to enable to easily process these commands as jobs. It also enables starting up indivudal
   servers if required.
.EXAMPLE
   Start-NeoloadGenerator -Name 'NeoloadGenerator13440'
.EXAMPLE
  @('Server1', 'Server2') | Start-NeoloadGenerator
#>
function Start-NeoloadGenerator
{
    Param
    (
        # Neoload Generator Name
        [Parameter(Mandatory=$true,
                   ValueFromPipeline,
                   Position=0)]
        $Name
    )

    Begin
    {
      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm
      $resTag = $neoloadData.ResourceTag
      $resGroup = $neoloadData.ResourceGroupName

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      }
    }
    Process
    {
      # Get VM Object

      Write-Verbose "Retrieving VM Object for $Name"
      $neoloadVM = Get-AzureRMVM -ResourceGroupName $resGroup -Name $Name

      # Startup VM

      Write-Verbose "Starting up $Name"
      return $neoloadVM | Start-AzureRmVM

    }
    End
    {
    }
}

<#
.Synopsis
   Completely remove a Neoload Generator from Azure
.DESCRIPTION
   Completely remove a Neoload Generator from Azure. This has been created as a mechanism
   to enable to easily process these commands as jobs. It also enables shutting down indivudal
   servers if required.
.EXAMPLE
   Remove-NeoloadGenerator -Name 'NeoloadGenerator13440'
.EXAMPLE
  @('Server1', 'Server2') | Remove-NeoloadGenerator
#>
function Remove-NeoloadGenerator
{
    [CmdletBinding()]
    Param
    (
        # Name of server
        [Parameter(Mandatory,
                   ValueFromPipeline,
                   Position=0)]
        $Name
    )

    Begin
    {
      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm
      $resTag = $neoloadData.ResourceTag
      $resGroup = $neoloadData.ResourceGroupName

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      } 

    }
    Process
    {
      # Get VM Object and NICs associated with VM

      Write-Verbose "Retrieving VM Object for $Name"
      $neoloadVM = Get-AzureRMVM -ResourceGroupName $resGroup -Name $Name

      Write-Verbose "Retrieving VM Disk Object for $Name"
      $neoloadDisk = Get-AzureRMDisk -ResourceGroupName $resGroup -DiskName $neoloadVM.StorageProfile.OsDisk.Name
      
      Write-Verbose "Retrieving NIC Objects for $Name"
      $nics = Find-AzureRmResource -TagName 'Owner' -TagValue 'ECOMM-Neoload' | Where-Object { $_.ResourceType -eq 'Microsoft.Network/networkInterfaces' } | Get-AzureRmNetworkInterface | Where-Object { $_.VirtualMachine.Id -like "*$Name*"}

      # Shutdown VM

      Write-Verbose "Removing $Name"
      $null = $neoloadVM | Remove-AzureRmVM -Force

      Write-Verbose "Removing Network Adapters for $Name"
      $null = $nics | Remove-AzureRmNetworkInterface -Force
      
      Write-Verbose "Removing OS Disk for $Name"
      $null = $neoloadDisk | Remove-AzureRMDisk -Force
      
    }
    End
    {
    }
}


<#
.Synopsis
   This cmdlet will shutdown all servers in the Neoload farm. If the -Deprovision switch is specified then it will also delete all servers.
.DESCRIPTION
   This cmdlet will shutdown all servers in the Neoload farm. If the -Deprovision switch is specified then it will also delete all servers.
.EXAMPLE
   Stop-NeoloadFarm
.EXAMPLE
   Stop-NeoloadFarm -Deprovision
#>
function Stop-NeoloadFarm
{
    [CmdletBinding()]
    Param
    (
        # Deprovision switch
        [switch]$Deprovision = $false

    )

    Begin
    {
    }
    Process
    {
      
      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm
      
      $resTag = $neoloadData.ResourceTag
      $resGroup = $neoloadData.ResourceGroupName

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      }  

      # Get a list of all VMs in the farm
      $neoloadVMs = Get-NeoloadFarm | Get-AzureRmVM

      Write-Verbose "$($neoloadVMs.count) servers found in Neoload Farm."

      # Start stopping VMs as jobs otherwise it is done sequentially and will take a long time.
      
      $stopJobs = @()

      Write-Verbose "Starting server shutdown process..."
      foreach($vm in $neoloadVMs)
      {
        $vmName = $vm.name
        $scriptRoot = $PSScriptRoot
        Write-Verbose "Starting shutdown job for $vmName"
        $stopJobs += Start-Job { . "$Using:scriptRoot\NeoloadModule.ps1" 
                                    Stop-NeoloadGenerator -Name $Using:vmName -Verbose }
      
      }

      # Set a max timeout waiting for VMs to stop in seconds.
      $TIMETOWAIT = 600                     # Ten minutes
      $LOOPTIME = 10                        # How long to wait between loops in seconds

      $timeWaited = 0

      while($stopJobs.State -contains 'Running' -and $timeWaited -lt $TIMETOWAIT)
      {
        
        $removeJobs = @()
        
        Write-Verbose "Waiting for servers to shutdown..."
        Start-Sleep -Seconds $LOOPTIME
        $timeWaited += $LOOPTIME
      }

      if($timeWaited -ge $TIMETOWAIT)
      {
        Write-Warning "Time-out waiting for servers to shutdown. Please check if servers are shutting down or try again."
        if($Deprovision) { Write-Warning 'Deprovision actions will not proceed as servers need to be shutdown first.' }
        Break
      }
      

      if($Deprovision)
      {
        Write-Verbose "Waiting 20 seconds for Deallocation to be complete."
        Start-Sleep -Seconds 20
        
        Write-Verbose "Starting server removal process..."
        foreach($vm in $neoloadVMs)
        {
          $vmName = $vm.name
          $scriptRoot = $PSScriptRoot
          Write-Verbose "Starting removal job for $vmName"
          $removeJobs += Start-Job { . "$Using:scriptRoot\NeoloadModule.ps1" 
                                      Remove-NeoloadGenerator -Name $Using:vmName -Verbose }
      
        }

        # Set a max timeout waiting for VMs to stop in seconds.
        $TIMETOWAIT = 600                     # Ten minutes
        $LOOPTIME = 10                        # How long to wait between loops in seconds

        $timeWaited = 0

        while($removeJobs.State -contains 'Running' -and $timeWaited -lt $TIMETOWAIT)
        {
          Write-Verbose "Waiting for servers to be removed..."
          Start-Sleep -Seconds $LOOPTIME
          $timeWaited += $LOOPTIME
        }

        if($timeWaited -ge $TIMETOWAIT)
        {
          Write-Warning "Time-out waiting for servers to be removed. Please check if servers are being removed."
        }  
      }

      # Clean up job data
      Get-Job | Where-Object { $_.State -ne 'Running' } | Remove-Job
      [gc]::Collect()

      # Return the current farm status
      return Get-NeoloadFarm


    }
    End
    {
    }
}


<#
.Synopsis
   This function allows you to set the size of the running farm.
.DESCRIPTION
   This function allows you to set the size of the running farm.
   If the farm is larger than the size specified, then it will shutdown some VMs.
   If the farm is smaller than the size specified, then it will start up servers up to
   a maximum of the number of servers available.
.EXAMPLE
   Set-NeoloadFarmSize -NumberofLoadGenerators 20
#>
function Set-NeoloadFarmSize
{
    [CmdletBinding()]
    Param
    (
        # NumberofLoadGenerators
        [Parameter(Mandatory,
                   Position=0)]
        [int]$NumberofLoadGenerators
    )

    Begin
    {
    }
    Process
    {

      # Connect to Azure and load required config values
      $neoloadData = Connect-NeoloadFarm
      
      $resTag = $neoloadData.ResourceTag
      $resGroup = $neoloadData.ResourceGroupName

      # If not data is returned by the connect commandlet then connection has failed so halt processing
      if( -not $neoloadData)
      {
        Throw 'Connection to Azure failed.'
        Break
      }
      
      $neoloadVMs = @(Get-NeoloadFarm)
      $neoloadVMCount = $neoloadVMs.count
      $neoloadVMStarted = @($neoloadVMs | Where-Object { $_.Status -eq 'VM Running' })
      $neoloadVMStartedCount = $neoloadVMStarted.count
      $neoloadVMStopped = @($neoloadVMs | Where-Object { $_.Status -eq 'VM deallocated' })
      $neoloadVMStoppedCount = $neoloadVMStopped.count

      Write-Verbose "$neoloadVMCount servers currently in the farm."  
      Write-Verbose "$neoloadVMStartedCount servers currently running."
      Write-Verbose "$neoloadVMStoppedCount servers currently stopped."
      

      if($NumberofLoadGenerators -eq $neoloadVMStartedCount)
      {
        Write-Verbose "The number of generators requested is the number of generators currently running."
        return $neoloadVMs
      }

      # If the number of running generators is less than what is currently running, then start some.
      if($NumberofLoadGenerators -gt $neoloadVMStartedCount)
      {
        $numberToGrow = $NumberofLoadGenerators - $neoloadVMStartedCount

        Write-Verbose "More generators need to be started. $numberToGrow more will needed to fulfill requirements."

        $numberToStart = $numberToGrow

        # If there are not enough stopped VMs to fulfill requirements, print a warning
        if($neoloadVMStoppedCount -lt $numberToGrow)
        {
          $numberToStart = $neoloadVMStoppedCount
          $numberToProvision = $numberToGrow - $numberToStart
          Write-Warning "Not enough stopped VMs to fulfill requirements. $numberToProvision more VMs required. Only $numberToStart will be started. Please provision more using New-NeoloadGenerator."

        }
        
        Write-Verbose "Will attempt to start $numberToStart"

        $vmsToStart = $neoloadVMStopped | Select-Object -First $numberToStart
        
        # Start powering on VMs as jobs otherwise it is done sequentially and will take a long time.
      
        $startJobs = @()

        Write-Verbose "Starting server power on process..."
        foreach($vm in $vmsToStart)
        {
          $vmName = $vm.name
          $scriptRoot = $PSScriptRoot
          Write-Verbose "Starting power on job for $vmName"
          $startJobs += Start-Job { . "$Using:scriptRoot\NeoloadModule.ps1" 
                                      Start-NeoloadGenerator -Name $Using:vmName -Verbose }
      
        }

        # Set a max timeout waiting for VMs to stop in seconds.
        $TIMETOWAIT = 300                     # Five minutes
        $LOOPTIME = 10                        # How long to wait between loops in seconds

        $timeWaited = 0

        while($startJobs.State -contains 'Running' -and $timeWaited -lt $TIMETOWAIT)
        {
        
          $removeJobs = @()
        
          Write-Verbose "Waiting for servers to power on..."
          Start-Sleep -Seconds $LOOPTIME
          $timeWaited += $LOOPTIME
        }

        if($timeWaited -ge $TIMETOWAIT)
        {
          Write-Warning "Time-out waiting for servers to power on. Please check if servers are powering on or try again."
          
          # Clean up job data
          Get-Job | Where-Object { $_.State -ne 'Running' } | Remove-Job
          [gc]::Collect()
          
          return Get-NeoloadFarm
        }



      }






      # If the number of needed generators is less than what is currently running, then shut some down.
      elseif($NumberofLoadGenerators -lt $neoloadVMStartedCount)
      {
        $numberToStop = $neoloadVMStartedCount - $NumberofLoadGenerators

        Write-Verbose "Some generators need to be stopped. $numberToStop will be stopped."

        $generatorsToStop = $neoloadVMStarted | Select-Object -First $numberToStop

        # Start stopping VMs as jobs otherwise it is done sequentially and will take a long time.
      
        $stopJobs = @()

        Write-Verbose "Starting server shutdown process..."
        foreach($vm in $generatorsToStop)
        {
          $vmName = $vm.name
          $scriptRoot = $PSScriptRoot
          Write-Verbose "Starting shutdown job for $vmName"
          $stopJobs += Start-Job { . "$Using:scriptRoot\NeoloadModule.ps1" 
                                      Stop-NeoloadGenerator -Name $Using:vmName -Verbose }
      
        }

        # Set a max timeout waiting for VMs to stop in seconds.
        $TIMETOWAIT = 300                     # Five minutes
        $LOOPTIME = 10                        # How long to wait between loops in seconds

        $timeWaited = 0

        while($stopJobs.State -contains 'Running' -and $timeWaited -lt $TIMETOWAIT)
        {
        
          $removeJobs = @()
        
          Write-Verbose "Waiting for servers to shutdown..."
          Start-Sleep -Seconds $LOOPTIME
          $timeWaited += $LOOPTIME
        }

        if($timeWaited -ge $TIMETOWAIT)
        {
          Write-Warning "Time-out waiting for servers to shutdown. Please check if servers are shutting down or try again."
          
          # Clean up job data
          Get-Job | Where-Object { $_.State -ne 'Running' } | Remove-Job
          [gc]::Collect()
          
          return Get-NeoloadFarm
        }

      }

      
      # Clean up job data
      Get-Job | Where-Object { $_.State -ne 'Running' } | Remove-Job
      [gc]::Collect()
      

      return (Get-NeoloadFarm)
    }

    End
    {
    }
}